﻿using UnityEngine;
using System.Collections;

public class MoveAndRoll : MonoBehaviour {
    public float time;
	// Use this for initialization
	void Start () {
        StartCoroutine(Move(new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y)));
        StartCoroutine(Roll());
    }
	
	// Update is called once per frame
	void Update () {
	    
	}
    IEnumerator Move(Vector3 pos)
    {
        var lastPosition = pos;
        var distanse=Mathf.Sqrt(Mathf.Pow(lastPosition.x - transform.position.x,2) + Mathf.Pow(lastPosition.y - transform.position.y,2));
        var speed = distanse / time;
        var clock = time;
        Vector3 vector = new Vector3(lastPosition.x - transform.position.x, lastPosition.y - transform.position.y);
        while (true)
        {
            if (clock <= Time.deltaTime * speed)
            {
                transform.position = Vector3.MoveTowards(transform.position, lastPosition, clock);
                break;
            }
            transform.position = Vector3.MoveTowards(transform.position, lastPosition, Time.deltaTime * speed);
            yield return new WaitForSeconds(0);
            clock -= Time.deltaTime;
            
        }
       
        yield break;
    }
    IEnumerator Roll()
    {
        float speed = 360 / time;
        var clock = time;
        while (true)
        {
            transform.Rotate(Vector3.back * speed * Time.deltaTime);
            clock -= Time.deltaTime;
            if (clock <= 0)
            {
                transform.rotation = new Quaternion(0, 0, 0, 0);
                yield break;
            }
            yield return new WaitForSeconds(0);
        }
        
    }
}
