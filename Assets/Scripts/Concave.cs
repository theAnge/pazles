﻿using UnityEngine;
using System.Collections;

public class Concave : MonoBehaviour {
    public Convex convex;
    public Transform pazzle;
    
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "convex")
        {
            Convex con = collider.GetComponent<Convex>();
            if (con == convex)
            {   
                con.ToBridge(pazzle);
                Destroy(gameObject);
            }
        }
    }
}
