﻿using UnityEngine;
using System.Collections;


public static class Extension
{
    public static Transform RootFather(this Transform transform)
    {
        Transform target = transform;
        while (target.parent != null)
            target = target.parent;
        return target;
    }
    public static void ShowFront(this Transform[] transforms)
    {
        for (int i = 0; i < transforms.Length; i++)
        {
            if (transforms[i].tag == "pazzle")
                transforms[i].SendMessage("OrderPlus");
        }
    }
    public static void ShowBeck(this Transform[] transforms)
    {
        for (int i = 0; i < transforms.Length; i++)
        {
            if (transforms[i].tag == "pazzle")
                transforms[i].SendMessage("OrderMinus");
        }
    }
}

