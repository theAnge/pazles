﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Pazzle : MonoBehaviour {
    public Transform Image;
    SpriteRenderer sr;
    public int numPazzles;
    bool check;
    // Use this for initialization
    void Start () {
        sr = GetComponent<SpriteRenderer>();
    }
	
	// Update is called once per frame
	void Update () {

        if (check)
        {
            check = false;
            int i = 0;
            var children = transform.GetComponentsInChildren<Transform>();
            foreach (var item in children)
            {
                if (item.tag == "pazzle")
                    i++;
            }
            
            Debug.Log(i);
            if (i == numPazzles)
                HappyEnd(children, i);

        }
    }
    public void OrderPlus()
    {
        sr.sortingOrder=1;
    }
    public void OrderMinus()
    {
        sr.sortingOrder=0;
    }
    public void CheckEnd()
    {
        check = true;
        
    }
    void HappyEnd(Transform[] children, int num)
    {
        Vector3 sum=Vector3.zero;
        foreach (var item in children)
        {
            if (item.tag == "pazzle")
                sum += item.position;
        }
        var center = sum / num;
        Instantiate(Image, center, transform.rotation);
        gameObject.active = false;
        
    }
    
}
