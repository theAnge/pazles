﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {
    Vector3 prefPosition;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0))
        {
            prefPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D raycastHit2D = Physics2D.Raycast(new Vector2(prefPosition.x, prefPosition.y), Vector2.zero, 0f);
            if (raycastHit2D.collider != null&&raycastHit2D.collider.tag=="pazzle")
            {
                this.StartCoroutine(MovePazzle(raycastHit2D.transform.RootFather()));
            }
        }
    }
    IEnumerator MovePazzle(Transform target)
    {
        target.SendMessage("OrderPlus");
        var childes = target.GetComponentsInChildren<Transform>();
        
        childes.ShowFront();
        prefPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        while (Input.GetMouseButton(0))
        { 
            yield return new WaitForSeconds(0);
            Vector3 newPosition=Camera.main.ScreenToWorldPoint(Input.mousePosition);
            target.position = target.position - prefPosition + newPosition;
            prefPosition = newPosition;
        }
        target.SendMessage("OrderMinus");
        childes.ShowBeck();
        yield break;
        
    }
    public void StopMovePazzle()
    {
        StopAllCoroutines();
    }
}
